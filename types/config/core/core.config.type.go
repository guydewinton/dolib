package core_config_type

type CoreConfigStruct struct {
	Project CoreConfigProjectStruct `mapstructure:"project" validate:"required"`
	Host    CoreConfigHostStruct    `mapstructure:"host" validate:"required"`
	Store   CoreConfigStoreStruct   `mapstructure:"store" validate:"required"`
}

type CoreConfigProjectStruct struct {
	Domain string `mapstructure:"domain" validate:"required"`
	Email  string `mapstructure:"email" validate:"required"`
}

type CoreConfigHostStruct struct {
	Mounts map[string]CoreConfigHostModulesStruct `mapstructure:"mounts" validate:"required"`
}

type CoreConfigHostModulesStruct struct {
	Root    CoreConfigHostModulesRootStruct                      `mapstructure:"root"    validate:"required"`
	Modules map[string]CoreConfigHostModulesModulesScriptsStruct `mapstructure:"modules" validate:"required,dive,required"`
}

type CoreConfigHostModulesRootStruct struct {
	Path string `mapstructure:"path" validate:"required"`
}

type CoreConfigHostModulesModulesScriptsStruct struct {
	Path     string            `mapstructure:"path" validate:"required"`
	Repo     string            `mapstructure:"repo" validate:""`
	Contents map[string]string `mapstructure:"contents" validate:""`
}

//////////////////////////////////////////////////////////////////////////////////////

type CoreConfigStoreStruct struct {
	Gitlab CoreConfigStoreGitlabStruct `mapstructure:"gitlab" validate:"required"`
}

type CoreConfigStoreGitlabStruct struct {
	Group CoreConfigStoreGitlabGroupStruct `mapstructure:"group" validate:"required"`
}

type CoreConfigStoreGitlabGroupStruct struct {
	Id int `mapstructure:"id" validate:"required"`
}

///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////

type CoreConfigHostModulesModulesStruct struct {
	Docker  CoreConfigHostModulesModulesDockerStruct  `mapstructure:"docker" validate:"required"`
	Scripts CoreConfigHostModulesModulesScriptsStruct `mapstructure:"scripts" validate:"required"`
	Mounts  CoreConfigHostModulesModulesMountsStruct  `mapstructure:"mounts" validate:"required"`
	Tmp     CoreConfigHostModulesModulesTmpStruct     `mapstructure:"tmp" validate:"required"`
}

type CoreConfigHostModulesModulesDockerStruct struct {
	Path string `mapstructure:"path" validate:"required"`
	Repo string `mapstructure:"repo" validate:"required"`
}

type CoreConfigHostModulesModulesMountsStruct struct {
	Path string `mapstructure:"path" validate:"required"`
}

type CoreConfigHostModulesModulesTmpStruct struct {
	Path string `mapstructure:"path" validate:"required"`
}
