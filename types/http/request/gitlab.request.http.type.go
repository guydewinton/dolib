package gitlab_request_type

type Gitlab_OAuthRequest struct {
	GrantType string `json:"grant_type"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type Gitlab_OAuthResponse struct {
	AccessToken string `json:"access_token"`
	TokenType string `json:"token_type"`
	ExpiresIn int `json:"expires_in"`
}

type Gitlab_DeployTokenResponse struct {
	Id int `json:"id"`
	Name string `json:"name"`
	Username string `json:"username"`
	Expires_at string `json:"expires_at"`
	Scopes []string `json:"scopes"`
	Revoked bool `json:"revoked"`
	Expired bool `json:"expired"`
	Token string `json:"token"`
}

type Gitlab_DeployTokenRequest struct {
	Name string `json:"name"`
	Scopes []string `json:"scopes"`
	ExpiresAt string `json:"expires_at"`
}

type Gitlab_GetCustomAttribute struct {
	Key string `json:"key"`
	Value string `json:"value"`
}

type Gitlab_SetCustomAttribute struct {
	Value string `json:"value"`
}

type Gitlab_UserResponse struct {
	Id int `json:"id"`
    Username string `json:"username"`
    Name string `json:"name"`
    State string `json:"state"`
}