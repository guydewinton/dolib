package auth_service

import (
	oauth_gitlab_request "gitlab.com/guydewinton/dodo/functions/http/request/gitlab/oauth"
	deploy_token_gitlab_request "gitlab.com/guydewinton/dodo/functions/http/request/gitlab/token/deploy"
	"time"
)

func GitlabBasicAuth() (username string, token string, time time.Time) {
	return oauth_gitlab_request.Gitlab_OAuthRequest()
}

func GitlabDeployAuth() (deployAccessUser string, deployAccessToken string) {
	username, currentTime, accessToken := oauth_gitlab_request.Gitlab_OAuthRequest()

	return deploy_token_gitlab_request.Gitlab_DeployTokenRequest(username, accessToken, currentTime)
}
