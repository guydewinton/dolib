package scripts_enum

var (
	HostInit     = "host-init.sh"
	DeployUp     = "deploy-up.sh"
	DeployDown   = "deploy-down.sh"
	StateRestore = "state-restore.sh"
	StateBackup  = "state-backup.sh"
)
