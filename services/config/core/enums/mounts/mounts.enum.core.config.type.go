package mounts_enum

const (
	Docker   = "docker"
	Scripts  = "scripts"
	Transfer = "transfer"
	Volumes  = "volumes"
)

const (
	volumeData = "data"
	volumeFile = "file"
)
