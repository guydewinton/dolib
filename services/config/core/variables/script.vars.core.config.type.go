package vars_core_config_type

import (
	"gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/system/config/core/enums/mounts"
	config_system "gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/system/config/core/yaml"
)

var ScriptsPath = config_system.CoreConfig.Host.Modules.Root.Path + config_system.CoreConfig.Host.Modules.Modules[mounts_enum.Scripts].Path
