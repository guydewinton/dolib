package yaml

import "C"
import (
	"bytes"
	"fmt"
	"github.com/go-playground/validator/v10"
	"github.com/mitchellh/mapstructure"
	"github.com/spf13/viper"
	config_embed "gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/system/embed/config"
	core_config_type "gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/types/config/core"
	"reflect"
	"strings"
)

var validate *validator.Validate

var CoreConfig = &core_config_type.CoreConfigStruct{}

type DotSeparatedStringList []string

func SometHookFunc() mapstructure.DecodeHookFunc {
	fmt.Println("lalala")
	// Wrapped in a function call to add optional input parameters (eg. separator)
	return func(
		f reflect.Type,
		t reflect.Type,
		data interface{},
	) (interface{}, error) {
		// Check that the data is string
		if f.Kind() != reflect.String {
			return data, nil
		}

		// Check that the target type is our custom type
		if t != reflect.TypeOf(DotSeparatedStringList{}) {
			return data, nil
		}

		// Return the parsed value
		return DotSeparatedStringList(strings.Split(data.(string), ".")), nil
	}
}

func ReadInCoreConfig() {
	viper.SetConfigType("yaml")
	err := viper.ReadConfig(bytes.NewBuffer(config_embed.Config))
	if err != nil {
		fmt.Printf(err.Error())
	}
	fmt.Println("here")
	validate = validator.New()
	viper.Unmarshal(CoreConfig)

	fmt.Println(CoreConfig.Host.Modules.Mounts["scripts"].Contents["restarthost"])

	if err := validate.Struct(CoreConfig); err != nil {
		if _, ok := err.(*validator.InvalidValidationError); ok {
			fmt.Println("errrroooorrrr")
			fmt.Println(err)
			return
		}

		for _, err := range err.(validator.ValidationErrors) {

			fmt.Println("Namespace", err.Namespace()) // can differ when a custom TagNameFunc is registered or
			fmt.Println("Field", err.Field())         // by passing alt name to ReportError like below
			fmt.Println("StructNamespace", err.StructNamespace())
			fmt.Println("StructField", err.StructField())
			fmt.Println("Tag", err.Tag())
			fmt.Println("ActualTag", err.ActualTag())
			fmt.Println("Kind", err.Kind())
			fmt.Println("Type", err.Type())
			fmt.Println("Value", err.Value())
			fmt.Println("Param", err.Param())
			fmt.Println()
		}

		// from here you can create your own error messages in whatever language you wish
		return
	}

}

// Create a new config instance.
