package tar_cmd_ssh_service

import (
	zip_cmd "gitlab.com/guydewinton/dodo/functions/cmd/zip"
	fs_service "gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/services/path"
	config_system "gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/system/config/core/yaml"
	mounts_enum "gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/types/config/core/enums/mounts"
)

type DirMapStruct struct {
	Path string
}

func UnzipAll(sourceKey string) {
	var target = fs_service.PathResolver(config_system.CoreConfig.Host.Modules.Mounts[mounts_enum.Mounts].Path)

	var source = fs_service.PathResolver(config_system.CoreConfig.Host.Modules.Mounts[mounts_enum.Tmp].Path) + sourceKey
	zip_cmd.UnZipCmd(source, target)
}

func UnzipDb(sourceKey string) {
	var target = fs_service.PathResolver(config_system.CoreConfig.Host.Modules.Mounts[mounts_enum.Mounts].Path) + "db"
	var source = fs_service.PathResolver(config_system.CoreConfig.Host.Modules.Mounts[mounts_enum.Tmp].Path) + sourceKey
	zip_cmd.UnZipCmd(source, target)
}

func UnzipFiles(sourceKey string) {
	var target = fs_service.PathResolver(config_system.CoreConfig.Host.Modules.Mounts[mounts_enum.Mounts].Path) + "file"
	var source = fs_service.PathResolver(config_system.CoreConfig.Host.Modules.Mounts[mounts_enum.Tmp].Path) + sourceKey
	zip_cmd.UnZipCmd(source, target)
}
