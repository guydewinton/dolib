package tar_cmd_ssh_service

import (
	zip_cmd "gitlab.com/guydewinton/dodo/functions/cmd/zip"
	fs_service "gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/services/path"
	config_system "gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/system/config/core/yaml"
)

func ZipAll(sourceKey string) {
	var source = fs_service.PathResolver(config_system.CoreConfig.Host.Modules.Mounts["mounts"].Path)
	var target = fs_service.PathResolver(config_system.CoreConfig.Host.Modules.Mounts["tmp"].Path) + sourceKey
	zip_cmd.ZipCmd(source, target)
}

func ZipDb(sourceKey string) {
	var source = fs_service.PathResolver(config_system.CoreConfig.Host.Modules.Mounts["mounts"].Path) + "db"
	var target = fs_service.PathResolver(config_system.CoreConfig.Host.Modules.Mounts["tmp"].Path) + sourceKey
	zip_cmd.ZipCmd(source, target)
}

func ZipFiles(sourceKey string) {

}
