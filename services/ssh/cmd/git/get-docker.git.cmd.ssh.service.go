package git_cmd_ssh_service

import (
	"fmt"
	"github.com/spf13/viper"
	git_cmd_function "gitlab.com/guydewinton/dodo/functions/cmd/git"
	ssh_shell "gitlab.com/guydewinton/dodo/functions/ssh/shell"
	"golang.org/x/crypto/ssh"
	"os"
)

func GetDockerRepo(connection *ssh.Client, deployAccessUser string, deployAccessToken string) {
	fmt.Println()
	fmt.Println("Pulling Docker Repo")
	ssh_shell.CmdSsh(connection, git_cmd_function.GitCloneOrPullTokenCmd(deployAccessUser, deployAccessToken, "gitlab.com", viper.GetString("repos.docker"), "/code/docker/"), os.Stdout)

}
