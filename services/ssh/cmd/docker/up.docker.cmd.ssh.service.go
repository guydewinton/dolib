package docker_cmd_ssh_service

import (
	util_cmd_function "gitlab.com/guydewinton/dodo/functions/cmd/util"
	ssh_shell "gitlab.com/guydewinton/dodo/functions/ssh/shell"
	"gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/system/scripts"
	"golang.org/x/crypto/ssh"
	"os"
)

func DockerUpCmdSshService(connection *ssh.Client, deployAccessUser string, deployAccessToken string) {
	var scriptList = []string{
		script_repo_exec.RepoScriptCmd_LoginDocker("/code/scripts/", deployAccessUser, deployAccessToken),
		script_repo_exec.RepoScriptCmd_StopDocker("/code/scripts/", "/code/docker/docker-compose.yml"),

		script_repo_exec.RepoScriptCmd_StartDocker("/code/scripts/", "/code/docker/docker-compose.yml"),
		script_repo_exec.RepoScriptCmd_LogoutDocker("/code/scripts/"),
	}

	ssh_shell.CmdSsh(connection, util_cmd_function.CmdSetListToString(scriptList), os.Stdout)
}
