package docker_cmd_ssh_service

import (
	util_cmd_function "gitlab.com/guydewinton/dodo/functions/cmd/util"
	ssh_shell "gitlab.com/guydewinton/dodo/functions/ssh/shell"
	script_repo_exec "gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/system/scripts"
	"golang.org/x/crypto/ssh"
	"os"
)

func DockerDownCmdSshService(connection *ssh.Client) {
	var scriptList = []string{
		script_repo_exec.RepoScriptCmd_StopDocker("/code/scripts/", "/code/docker/docker-compose.yml"),
	}

	ssh_shell.CmdSsh(connection, util_cmd_function.CmdSetListToString(scriptList), os.Stdout)
}
