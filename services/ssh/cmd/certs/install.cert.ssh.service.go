package certs_cmd_ssh

import (
	ssh_shell "gitlab.com/guydewinton/dodo/functions/ssh/shell"
	config_system "gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/system/config/core/yaml"
	script_repo_exec "gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/system/scripts"
	"golang.org/x/crypto/ssh"
	"os"
)

func InstallCertsCmdSsh(connection *ssh.Client) {
	ssh_shell.CmdSsh(
		connection,
		script_repo_exec.RepoScriptCmd_InstallCertificates("/code/scripts/", config_system.CoreConfig.Project.Domain, config_system.CoreConfig.Project.Email),
		os.Stdout,
	)
}
