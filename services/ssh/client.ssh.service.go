package ssh_service

import (
	"github.com/spf13/viper"
	ssh_exec "gitlab.com/guydewinton/dodo/functions/ssh"
	certs_embed "gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/system/embed/certs"
	"golang.org/x/crypto/ssh"
)

func SshClient() *ssh.Client {
	return ssh_exec.ConnectSsh(viper.GetString("domain.url"), 22, certs_embed.SshPrivateKey)

}
