package path_service

import (
	config_system "gitlab.com/lemmman_project_root/utilities/deploy-utility/clients/cli/system/config/core/yaml"
	"strings"
)

func PathResolver(modulePath string) string {
	var basePathArray = strings.Split(config_system.CoreConfig.Host.Modules.Root.Path, "/")
	var modulePathArray = strings.Split(modulePath, "/")
	var combinedPathArray = append(basePathArray, modulePathArray...)
	return "/" + strings.Join(combinedPathArray, "/") + "/"
}
